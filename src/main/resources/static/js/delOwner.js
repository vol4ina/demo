/**
 * Created by osevryukov on 6/13/16.
 */

$(document).ready(function () {
    $('.ownerdel_field').click(function () {
        var owner = {
            "owner": $(this).val()
        };
        var ownerName = $(this).val();
        var delOwner = confirm('Delete ' + ownerName + ' ?');
        if (delOwner == true) {
            if (ownerName != "Free" && ownerName != "Sold") {
                $.ajax({
                    type: "POST",
                    url: "/delowner",
                    cache: false,
                    data: JSON.stringify(owner),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: onAjaxSuccess(ownerName)
                });
            } else {
                alert("Owner " + ownerName + " is Protected!!!")
            }

        } else {
            alert("Skip")
        }
        window.location.reload();
    });
    $('.pagerefresh').click(function () {
        window.location.reload();
    })

});
function onAjaxSuccess(data) {
    window.location.reload();
    alert("Owner " + data + " was deleted !!!")
}

