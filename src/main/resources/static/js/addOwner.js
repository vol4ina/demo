/**
 * Created by osevryukov on 6/13/16.
 */

$(document).ready(function () {
    $('.owner_field').on('change', function () {

        var id = $(this).attr('id');
        id = id.split('_').pop();

        var owner = {
            "id": id,
            "owner": $(this).val(),
        };
        $.ajax({
            type: "POST",
            url: "/addowner",
            data: JSON.stringify(owner),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: onAjaxSuccess(owner)
        });
    });
});

function onAjaxSuccess(data) {
    alert('Owner was added');
}