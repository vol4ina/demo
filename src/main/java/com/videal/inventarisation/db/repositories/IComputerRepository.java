package com.videal.inventarisation.db.repositories;

import com.videal.inventarisation.db.models.Computers;
import com.videal.inventarisation.db.models.Owner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;




/**
 * Created by oleg on 11/9/15.
 */
@Transactional
@Repository
public interface IComputerRepository extends CrudRepository<Computers, Long>{

   Computers findByMotherboardsnLike(String motherboardsn);
   List<Computers> findAll();
   List<Computers> findByOwnerLike(Owner owner);
   List<Computers> findByOwnerLike(String owner);
   List<Computers> findAllByMotherboardsnAndMotherboardprname(String motherboardsn, String motherboardprname);

   List<Computers> findByOsOrHostnameOrCpu(String q1, String q2, String q3);
   List<Computers> findById(Long id);
}


