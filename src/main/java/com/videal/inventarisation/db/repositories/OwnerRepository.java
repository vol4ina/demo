package com.videal.inventarisation.db.repositories;

import com.videal.inventarisation.db.models.Owner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by oleg on 10/22/15.
 */
@Repository
public interface OwnerRepository extends CrudRepository<Owner, Long> {
   List<Owner> findAll();

   List<Owner> findByOwnerLike(String ownname);

   Owner findOneByOwnerLike(String ownerName);
}
