/**
 * Created by oleg on 10.08.15.
 */
package com.videal.inventarisation.db.repositories;

import com.videal.inventarisation.db.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}