package com.videal.inventarisation.db.repositories;

import com.videal.inventarisation.db.models.Computers;
import com.videal.inventarisation.db.models.Ram;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by oleg on 10/22/15.
// */
@Repository

//public class RamRepository extends EntityService{
//
//    public Ram add(Ram ram) {
//        entityManager.getTransaction().begin();
//        Ram ramFromDB = entityManager.merge(ram);
//        entityManager.getTransaction().commit();
//        entityManager.flush();
//        return ramFromDB;
//    }
//
//    public Ram get(long ram_id){return entityManager.find(Ram.class, ram_id);}
//
//    public Ram getRam(String ram){return entityManager.find(Ram.class, ram);}
//
//    public void deleteRamId(long ram_id) {
//        entityManager.getTransaction().begin();
//        entityManager.remove(get(ram_id));
//        entityManager.getTransaction().commit();
//    }
//
//    public void updateRam(Ram ram){
//        entityManager.getTransaction().begin();
//        entityManager.merge(ram);
//        entityManager.getTransaction().commit();
//    }
//
//    public List<Ram> getAll(){
//       TypedQuery<Ram> namedQuery = entityManager.createNamedQuery("Ram.getAll", Ram.class);
//        return namedQuery.getResultList();
//    }
//
//}

public interface RamRepository extends CrudRepository <Ram, Long>{
    List<Ram> findAll ();
    List<Ram> findByComputersOrderBySlotAsc(Computers computer);
}
