package com.videal.inventarisation.db.repositories;

import com.videal.inventarisation.db.models.Computers;
import com.videal.inventarisation.db.models.Hdd;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Set;

/**
 * Created by oleg on 10/27/15.
 */
@Repository
public interface HddRepository extends CrudRepository<Hdd, Long> {
    List<Hdd> findAll ();
    List<Hdd> findByComputersOrderBySerialAsc(Computers computer);
    List<Hdd> findByComputersLike(Computers computer);

}

