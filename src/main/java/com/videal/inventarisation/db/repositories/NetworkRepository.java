package com.videal.inventarisation.db.repositories;

import com.videal.inventarisation.db.models.Computers;
import com.videal.inventarisation.db.models.Network;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by oleg on 10/27/15.
 */
@Repository
public interface NetworkRepository extends CrudRepository<Network, Long> {
    List<Network> findAll();
    List<Network> findByComputersLike(Computers computer);
}
