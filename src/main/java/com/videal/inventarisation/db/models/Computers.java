package com.videal.inventarisation.db.models;

import javax.persistence.*;
import java.util.*;

/**
 * Created by oleg on 23.04.15.
 */
@Entity
@NamedQuery(name = "Computers.getAll", query = "SELECT c from Computers c")
@Table(name = "computers")
public class Computers {

    public Computers() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comp_id")
    protected Long id;
    private String hostname;
    private String os;
    private String systemmanufacturer;
    private String pcname;
    private String pcsn;
    private String motherboardsn;
    private String motherboardprname;
    private String cpu;
    private String uuid;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "owner")
    private Owner owner;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "comp_id")
    private List<Ram> ram = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "comp_id")
    private List<Hdd> hdd = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "comp_id")
    private List<Network> network = new ArrayList<>();

    public String getHostname() {        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getSystemmanufacturer() {
        return systemmanufacturer;
    }

    public void setSystemmanufacturer(String systemmanufacturer) {
        this.systemmanufacturer = systemmanufacturer;
    }

    public String getPcname() {
        return pcname;
    }

    public void setPcname(String pcname) {
        this.pcname = pcname;
    }

    public String getPcsn() {
        return pcsn;
    }

    public void setPcsn(String pcsn) {
        this.pcsn = pcsn;
    }

    public String getMotherboardsn() {
        return motherboardsn;
    }

    public void setMotherboardsn(String motherboardsn) {
        this.motherboardsn = motherboardsn;
    }

    public String getMotherboardprname() {
        return motherboardprname;
    }

    public void setMotherboardprname(String motherboardprname) {
        this.motherboardprname = motherboardprname;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public List<Ram> getRam() {
        return ram;
    }

    public void setRam(List<Ram> ram) {
        this.ram = ram;
    }

    public List<Hdd> getHdd() {
        return hdd;
    }

    public void setHdd(List<Hdd> hdd) {
        this.hdd = hdd;
    }

    public List<Network> getNetwork() {
        return network;
    }

    public void setNetwork(List<Network> network) {
        this.network = network;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
