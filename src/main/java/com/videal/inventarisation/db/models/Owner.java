package com.videal.inventarisation.db.models;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by oleg on 10/22/15.
 */

@Entity
@NamedQuery(name = "Owner.getAll", query = "SELECT ow from Owner ow")
@Table(name = "owner")
public class Owner {

    public Owner(){}
    @Id
    @Column(name = "owner")
    private String owner;

    @OneToMany(mappedBy = "owner",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Set<Computers> computers = new HashSet<Computers>();

    public Owner(String owner, Set<Computers> computers) {
        this.owner = owner;
        this.computers = computers;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }


    public void setComputers(Set<Computers> computers) {
        this.computers = computers;
    }

}

