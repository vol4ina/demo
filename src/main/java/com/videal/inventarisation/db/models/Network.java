package com.videal.inventarisation.db.models;

import javax.persistence.*;

/**
 * Created by oleg on 11/9/15.
 */
@Entity
@NamedQuery(name = "Network.getAll",query = "SELECT n FROM Network n")
@Table(name = "network")
public class Network extends BaseEntity{

    public Network() {}

    @Column(name = "net_interface")
    private String netinterface;
    @Column(name = "net_ip")
    private String ip;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "comp_id", updatable = false, insertable = false)
    private Computers computers;

    public Network(String netinterface, String ip, Computers computers) {
        this.netinterface = netinterface;
        this.ip = ip;
        this.computers = computers;
    }

    public String getNetinterface() {
        return netinterface;
    }

    public void setNetinterface(String netinterface) {
        this.netinterface = netinterface;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Computers getComputers() {
        return computers;
    }

    public void setComputers(Computers computers) {
        this.computers = computers;
    }
}
