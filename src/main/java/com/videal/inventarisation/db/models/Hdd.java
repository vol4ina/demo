package com.videal.inventarisation.db.models;
import javax.persistence.*;

/**
 * Created by oleg on 10/27/15.
 */

@Entity
@Table(name = "hdd")
@NamedQuery(name = "Hdd.getAll",query = "SELECT h FROM Hdd h")
public class Hdd extends BaseEntity {
    
    public Hdd(){}

    @Column(name = "hdd_size")
    private String size;

    @Column(name = "hdd_serial")
    private String serial;

    @Column(name = "hdd_name")
    private String product;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "comp_id",insertable = false, updatable = false)
    private Computers computers;

    public Hdd(String size, String serial, String product, Computers computers) {
        this.size = size;
        this.serial = serial;
        this.product = product;
        this.computers = computers;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Computers getComputers() {
        return computers;
    }

    public void setComputers(Computers computers) {
        this.computers = computers;
    }
}
