package com.videal.inventarisation.db.models;


import javax.persistence.*;

/**
 * Created by oleg on 10/27/15.
 */
@Entity
@NamedQuery(name = "Ram.getAll",query = "SELECT r FROM Ram r")
@Table(name = "ram")
public class Ram extends BaseEntity {

    public Ram(){}
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;

    @Column(name = "ram_serial")
    private String serial;
    @Column(name = "ram_size")
    private String size;
    @Column(name = "ram_type")
    private String type;
//    @Column(name = "ram_slot")
    private int slot;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comp_id",insertable = false, updatable = false)
    private Computers computers;

    public Ram(String size, String type, String serial, Computers computers, int slot) {
        this.size = size;
        this.type = type;
        this.serial = serial;
        this.computers = computers;
        this.slot = slot;
    }

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Computers getComputers() {
        return computers;
    }

    public void setComputers(Computers computers) {
        this.computers = computers;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }
}
