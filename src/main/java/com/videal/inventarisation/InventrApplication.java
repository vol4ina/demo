package com.videal.inventarisation;

import com.videal.inventarisation.db.models.Owner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;




@SpringBootApplication
@EnableAutoConfiguration
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.videal.inventarisation"})
@EnableJpaRepositories(basePackages = {"com.videal.inventarisation"})
public class InventrApplication extends WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter
{

    public static void main(String[] args) {
        SpringApplication.run(InventrApplication.class, args);
    }
}
