package com.videal.inventarisation.web;
//
///**
// * Created by oleg on 24.04.15.
// */
//
import com.videal.inventarisation.db.repositories.HddRepository;
import com.videal.inventarisation.db.repositories.IComputerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class HddController {

    @Qualifier("hddRepository")
    @Autowired
    private HddRepository hddRepository;

    @RequestMapping(value = "/hdd")
    public String showHdd(Map<String, Object> model) {
        model.put("hdd", hddRepository.findAll());
        return "hdd";
    }
}