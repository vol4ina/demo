package com.videal.inventarisation.web.model;

import javax.validation.constraints.NotNull;

/**
 * Created by osevryukov on 6/10/16.
 */
public class DelOwnerRequest {
    @NotNull
    String owner;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}

