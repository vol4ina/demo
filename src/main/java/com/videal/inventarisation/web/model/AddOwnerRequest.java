package com.videal.inventarisation.web.model;

/**
 * Created by osevryukov on 6/10/16.
 */
public class AddOwnerRequest {
    Long id;
    String owner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}

