package com.videal.inventarisation.web;
//
///**
// * Created by oleg on 24.04.15.
// */
//

import com.videal.inventarisation.db.repositories.IComputerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;


@Controller
public class ComputersController {

    @Qualifier("IComputerRepository")
    @Autowired
    private IComputerRepository computerRepository;

    @RequestMapping(value = "/")
    public String showComputers(Map<String, Object> model) {
        model.put("computers", computerRepository.findAll());
        return "computers";
    }

    @RequestMapping(value = "/computers")
    public String root(Map<String, Object> model) {
        model.put("computers", computerRepository.findAll());
        return "computers";
    }
}