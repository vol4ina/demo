package com.videal.inventarisation.web;
//
///**
// * Created by oleg on 24.04.15.
// */
//


import com.videal.inventarisation.db.repositories.OwnerRepository;
import com.videal.inventarisation.service.InventarisationServices;
import com.videal.inventarisation.web.model.AddOwnerRequest;
import com.videal.inventarisation.web.model.DelOwnerRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@Controller
public class OwnersController {
    @Autowired
    private InventarisationServices inventarisationServices;

    @Qualifier("ownerRepository")
    @Autowired
    private OwnerRepository ownerRepository;


    @RequestMapping(value = "/owners")
    public String showOwners(Map<String, Object> model) {
        model.put("owners", ownerRepository.findAll());
        return "owners";
    }

    @RequestMapping(value = "/addowner", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String addOwner(@RequestBody AddOwnerRequest addOwnerRequest) {
        inventarisationServices.addOwnerToComputer(addOwnerRequest);
        return "computers";
    }

    @RequestMapping(value = "/delowner", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String delOwner(@RequestBody DelOwnerRequest delOwnerRequest) {
        inventarisationServices.delOwner(delOwnerRequest);
        return "owners";
    }

}