package com.videal.inventarisation.web;
//
///**
// * Created by oleg on 24.04.15.
// */
//
import com.videal.inventarisation.db.repositories.RamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class RamController {

    @Qualifier("ramRepository")
    @Autowired
    private RamRepository ramRepository;

    @RequestMapping(value = "/ram")
    public String showRam(Map<String, Object> model) {
        model.put("ram", ramRepository.findAll());
        return "ram";
    }
}