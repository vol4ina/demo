package com.videal.inventarisation.web;
//
///**
// * Created by oleg on 24.04.15.
// */
//

import com.videal.inventarisation.db.repositories.IComputerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class SearchController {

    @Qualifier("IComputerRepository")
    @Autowired
    private IComputerRepository computerRepository;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String getComputerById(@RequestParam(value = "q", required = true) String query,
                                  Model model) {
//        try {
//
//            Long q = Long.valueOf(query);
//            model.addAttribute("computers", computerRepository.findById(q));
//
//
//        }catch (NumberFormatException e){
//            String q = query;
//            model.addAttribute("computers", computerRepository.findByOsOrHostname(q, q));
//        }
        if (query != null) {
            model.addAttribute("computers", computerRepository.findByOsOrHostnameOrCpu(query, query, query));
        }
        return "computers";
    }
}