package com.videal.inventarisation.web;
//
///**
// * Created by oleg on 24.04.15.
// */
//

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/logout")
    public String logout(){
        return "logout";
    }
}