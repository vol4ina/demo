package com.videal.inventarisation.service;

/**
 * Created by oleg on 24.04.15.
 */
public class Files {
    private String filename;

    public Files(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

}
