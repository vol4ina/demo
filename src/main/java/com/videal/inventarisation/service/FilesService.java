package com.videal.inventarisation.service;


import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by oleg on 24.04.15.
 */
@Component
public class FilesService {

    public File[] findFiles(String path) {

        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        return listOfFiles;
    }

    private static void exists(String nameFile) throws FileNotFoundException {
        File file = new File(nameFile);
        if (!file.exists()) {
            throw new FileNotFoundException(file.getName());
        }
    }

    public static void delete(String nameFile) throws FileNotFoundException {
        exists(nameFile);
        new File(nameFile).delete();
    }
}
