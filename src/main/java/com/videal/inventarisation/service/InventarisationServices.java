package com.videal.inventarisation.service;

import com.videal.inventarisation.db.models.*;
import com.videal.inventarisation.web.model.AddOwnerRequest;
import com.videal.inventarisation.web.model.DelOwnerRequest;
import org.springframework.dao.DataAccessException;

import java.util.Collection;
import java.util.List;

/**
 * Created by oleg on 12/22/15.
 */
//@Service
public interface InventarisationServices {

    String getHostByMotherboardSn(String motherboardsn) throws DataAccessException;

    Collection<Computers> getComputersByOwner(Owner owner) throws DataAccessException;

    void saveOwner(Owner owner) throws DataAccessException;

    void saveComputer(Computers computer) throws DataAccessException;

    void saveHdd(Hdd hdd) throws DataAccessException;

    void saveRam(Ram ram) throws DataAccessException;

    void editComputer(Computers computers) throws DataAccessException;

    void addOwnerToComputer(AddOwnerRequest owner) throws DataAccessException;

    Computers updateComputer(Computers computers) throws DataAccessException;

    List<Ram> updateRam(List<Ram> ram, Computers computer ) throws DataAccessException;

    List<Hdd> updateHdd(List<Hdd> hdd, Computers computer ) throws DataAccessException;

    List<Network> updateNetwork(List<Network> network, Computers computer ) throws DataAccessException;

    void delOwner(DelOwnerRequest owner) throws DataAccessException;

    boolean checkOwner(String ownerStatus) throws DataAccessException;


}
