package com.videal.inventarisation.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.videal.inventarisation.db.models.Computers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

/**
 * Created by oleg on 24.04.15.
 */

@Component
public class JsonDemidecodeParser implements IParser {

    public JsonDemidecodeParser() {
    }

    @Autowired
    FilesService filesService;

   @Autowired
//   @Qualifier("inventarisationServicesImpl")
    private InventarisationServices inventarisationServices;


    static ObjectMapper mapper = new ObjectMapper();

    @Override
    public void parse(String path) throws IParserException {

        try {
            for (File file : filesService.findFiles(path)) {
                if (file.isFile()) {
                    File rdFile = new File(path + file.getName());
//                    Owner owner = new Owner();
                    Computers comp = mapper.readValue(rdFile, Computers.class);
//                    owner.setOwner("Free");
//                    comp.setOwner(owner);
                    comp = inventarisationServices.updateComputer(comp);

                    comp.setRam(inventarisationServices.updateRam(comp.getRam(),comp));
                    comp.setHdd(inventarisationServices.updateHdd(comp.getHdd(),comp));
                    comp.setNetwork(inventarisationServices.updateNetwork(comp.getNetwork(),comp));

                    inventarisationServices.saveComputer(comp);
                }
                file.delete();
            }
        } catch (IOException e) {
            throw new IParserException(e);
        }
    }
}


