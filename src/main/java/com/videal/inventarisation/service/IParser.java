package com.videal.inventarisation.service;

/**
 * Created by oleg on 11/6/15.
 */
interface IParser {

    void parse(String path) throws IParserException;
}
