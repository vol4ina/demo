package com.videal.inventarisation.service;

/**
 * Created by oleg on 11/6/15.
 */
public class IParserException extends Exception {
    public IParserException() {
    }

    public IParserException(String message) {
        super(message);
    }

    public IParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public IParserException(Throwable cause) {
        super(cause);
    }

    public IParserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
