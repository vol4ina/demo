package com.videal.inventarisation.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.videal.inventarisation.db.models.*;
import com.videal.inventarisation.db.repositories.*;
import com.videal.inventarisation.web.model.AddOwnerRequest;
import com.videal.inventarisation.web.model.DelOwnerRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by oleg on 12/22/15.
 */

@Component(value = "inventarisationServicesImpl")
public class InventarisationServicesImpl implements InventarisationServices {

    @Qualifier("IComputerRepository")
    @Autowired
    private IComputerRepository computerRepository;
    @Qualifier("ownerRepository")
    @Autowired
    private OwnerRepository ownerRepository;
    @Qualifier("hddRepository")
    @Autowired
    private HddRepository hddRepository;
    @Autowired
    private RamRepository ramRepository;
    @Autowired
    private NetworkRepository networkRepository;

    static ObjectMapper mapper = new ObjectMapper();

    public InventarisationServicesImpl() {
    }

    @Override
    @Transactional(readOnly = true)
    public String getHostByMotherboardSn(String motherboardsn) {
        Computers computer = computerRepository.findByMotherboardsnLike(motherboardsn);
        return computer.getHostname();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Computers> getComputersByOwner(Owner owner) {
        List<Computers> comps = computerRepository.findByOwnerLike(owner);
        return comps;
    }

    @Override
    @Transactional
    public void saveOwner(Owner owner) throws DataAccessException {

            ownerRepository.save(owner);
    }

    @Override
    @Transactional
    public void saveComputer(Computers computer) throws DataAccessException {
        computerRepository.save(computer);
    }

    @Override
    @Transactional
    public void saveHdd(Hdd hdd) throws DataAccessException {
        hddRepository.save(hdd);
    }

    @Override
    @Transactional
    public void saveRam(Ram ram) throws DataAccessException {
        ramRepository.save(ram);
    }
    
    @Override
    public Computers updateComputer(Computers computers) {
        Owner freeOwner = new Owner();
        freeOwner.setOwner("Free");
        List<Computers> computersListFromDb = new ArrayList<>(computerRepository.findAllByMotherboardsnAndMotherboardprname(computers.getMotherboardsn(), computers.getMotherboardprname()));
        for (Computers compDb : computersListFromDb) {
            if (computers.getUuid().equals(compDb.getUuid()) & computers.getMotherboardsn().equals(compDb.getMotherboardsn()) & computers.getMotherboardprname().equals(compDb.getMotherboardprname()) & computers.getSystemmanufacturer().equals(compDb.getSystemmanufacturer())) {
                computers.setId(compDb.getId());
                if (!(compDb.getOwner() == null) && !checkOwner(compDb.getOwner().getOwner())) {
                    computers.setOwner(compDb.getOwner());
                } else {
                    computers.setOwner(freeOwner);
                }

            }
        }
        if (computers.getId() == null) {
            computers.setOwner(freeOwner);
            saveComputer(computers);
        }
        return computers;
    }

    @Override
    public List<Ram> updateRam(List<Ram> ramSet, Computers comp ) {
        List<Ram> ramListFromDb = new ArrayList<>(ramRepository.findByComputersOrderBySlotAsc(comp));
        List<Ram> ramListFromComp = new ArrayList<>(ramSet);

        //Make relationship between Ram and Computer - set computer to the ram object
        for (int i = 0; i < ramListFromComp.size();i++ ){
            Ram ram = ramListFromComp.get(i);
            ram.setComputers(comp);
        }

        //Search duplicate in DB and set id from db to the new Ram
        for (int i = 0; i < ramListFromDb.size(); i++) {
            String ramSerialFromDb = ramListFromDb.get(i).getSerial();
            Long ramIdFromDb = ramListFromDb.get(i).getId();
            String ramSizeFromDb = ramListFromDb.get(i).getSize();
            String ramTypeFromDb = ramListFromDb.get(i).getType();
            int ramSlotFromDB = ramListFromDb.get(i).getSlot();

            for (int j = 0; j < ramListFromComp.size(); j++) {
                String ramSerialFromComp = ramListFromComp.get(j).getSerial();
                String ramSizeFromComp = ramListFromComp.get(j).getSize();
                String ramTypeFromComp = ramListFromComp.get(j).getType();
                int ramSlotFromComp    = ramListFromComp.get(j).getSlot();
                if (ramSerialFromComp.equals(ramSerialFromDb) & ramSizeFromComp.equals(ramSizeFromDb) & ramTypeFromComp.equals(ramTypeFromDb) & ramSlotFromComp==ramSlotFromDB)  {
                    Ram ram = ramListFromComp.get(j);
                    ram.setId(ramIdFromDb);
                    ram.setComputers(ramListFromComp.get(i).getComputers());
                    ramListFromComp.set(j, ram);
                }
            }
        }
        List<Ram> targetRam = new ArrayList<>(ramListFromComp);
        return targetRam;
    }
    
    @Override
    public List<Hdd> updateHdd(List<Hdd> hddList, Computers comp ) {
        List<Hdd> hddListFromDb = new ArrayList<>(hddRepository.findByComputersLike(comp));
        List<Hdd> hddListFromComp = new ArrayList<>(hddList);

        //Make relationship between Hdd and Computer - set computer to the hdd object
        for (int i = 0; i < hddListFromComp.size();i++ ){
            Hdd hdd = hddListFromComp.get(i);
            hdd.setComputers(comp);
        }

        //Search duplicate in DB and set id from db to the new Hdd
        for (int i = 0; i < hddListFromDb.size(); i++) {
            String hddSerialFromDb = hddListFromDb.get(i).getSerial();
            Long hddIdFromDb = hddListFromDb.get(i).getId();
            String hddSizeFromDb = hddListFromDb.get(i).getSize();
            String hddPoductFromDb = hddListFromDb.get(i).getProduct();
            
            for (int j = 0; j < hddListFromComp.size(); j++) {
                String hddSerialFromComp = hddListFromComp.get(j).getSerial();
                String hddSizeFromComp = hddListFromComp.get(j).getSize();
                String hddProductFromComp = hddListFromComp.get(j).getProduct();
                if (hddSerialFromComp.equals(hddSerialFromDb) & hddSizeFromComp.equals(hddSizeFromDb) & hddProductFromComp.equals(hddPoductFromDb))  {
                    Hdd hdd = hddListFromComp.get(j);
                    hdd.setId(hddIdFromDb);
                    hdd.setComputers(hddListFromComp.get(i).getComputers());
                    hddListFromComp.set(j, hdd);
                }
            }
        }
        List<Hdd> targetHdd = new ArrayList<>(hddListFromComp);
        return targetHdd;
    }

    @Override
    public List<Network> updateNetwork(List<Network> networkList, Computers comp ) {
        List<Network> networkListFromDb = new ArrayList<>(networkRepository.findByComputersLike(comp));
        List<Network> networkListFromComp = new ArrayList<>(networkList);

        //Make relationship between Network and Computer - set computer to the network object
        for (int i = 0; i < networkListFromComp.size();i++ ){
            Network network = networkListFromComp.get(i);
            network.setComputers(comp);
        }

        //Search duplicate in DB and set id from db to the new Network
        for (int i = 0; i < networkListFromDb.size(); i++) {
            String networkNetinterfaceFromDb = networkListFromDb.get(i).getNetinterface();
            String networkIpFromDb = networkListFromDb.get(i).getIp();
            Long   networkIdFromDb = networkListFromDb.get(i).getId();
            for (int j = 0; j < networkListFromComp.size(); j++) {
                String networkNetinterfaceFromComp = networkListFromComp.get(j).getNetinterface();
                String networkIpFromComp = networkListFromComp.get(j).getIp();
                if (networkNetinterfaceFromComp.equals(networkNetinterfaceFromDb) & networkIpFromComp.equals(networkIpFromDb))  {
                    Network network = networkListFromComp.get(j);
                    network.setId(networkIdFromDb);
                    network.setComputers(networkListFromComp.get(i).getComputers());
                    networkListFromComp.set(j, network);
                }
            }
        }
        List<Network> targetNetwork = new ArrayList<>(networkListFromComp);
        return targetNetwork;
    }

    @Override
    public void editComputer(Computers computers) throws DataAccessException {
        Computers existComp = computerRepository.findByMotherboardsnLike(computers.getMotherboardsn());
        existComp.setOs(computers.getOs());
        computerRepository.save(existComp);
    }

    @Override
    public void addOwnerToComputer(AddOwnerRequest owner) throws DataAccessException {
        Computers computers = computerRepository.findOne(owner.getId());
        Owner owner1 = new Owner();

        owner1.setOwner(owner.getOwner());
        List<Owner> ownerList = new ArrayList<>(ownerRepository.findAll());
        for (Owner dbOwners : ownerList) {
            if (owner1.getOwner().equals(dbOwners.getOwner())) {
                computers.setOwner(dbOwners);
                break;
            } else {
                computers.setOwner(owner1);
            }
        }

//        updateComputer(computers);
        saveComputer(computers);
    }

    @Override
    public void delOwner(DelOwnerRequest owner) throws DataAccessException {
        String free = "Free";
        List<Owner> ownerList = ownerRepository.findByOwnerLike(owner.getOwner());
        if (!(owner.getOwner().equals("Free")) && (owner.getOwner() != null)) {
            for (Owner own : ownerList) {
                List<Computers> computersList = computerRepository.findByOwnerLike(own);
                for (Computers comp : computersList) {
                    if (ownerRepository.findOneByOwnerLike("Free") != null || !owner.getOwner().contains("Free")) {
                        comp.setOwner(ownerRepository.findOneByOwnerLike("Free"));
                    } else {
                        Owner freeOwner = new Owner();
                        freeOwner.setOwner("Free");
                        comp.setOwner(freeOwner);
                    }
                    saveComputer(comp);
                }
                ownerRepository.delete(ownerRepository.findByOwnerLike(owner.getOwner()));
            }
        } else {
            return;
        }
    }

    @Override
    public boolean checkOwner(String ownerStatus) throws DataAccessException {
        Pattern p = Pattern.compile("^[F,f,R,r,E,e]*$");
        Matcher m = p.matcher(ownerStatus);
        return m.matches();
    }

}
