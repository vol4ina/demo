package com.videal.inventarisation;

import java.io.*;

/**
 * Created by oleg on 10/28/15.
 */
public class StringParcer {

    private int lineNumber;
    private String path;
    private String newString;


    public StringParcer(int lineNumber, String path, String newString) {
        this.lineNumber = lineNumber;
        this.path = path;
        this.newString = newString;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getNewString() {
        return newString;
    }

    public void setNewString(String newString) {
        this.newString = newString;
    }

//    public void changeLine () throws IOException{
//        int lineNum = getLineNumber();
//        File file = new File(getPath());
//        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
//        String line = null;
//        ArrayList <String> list = new ArrayList<>();
//        while ((line = br.readLine())!=null ){
//                    list.add(line);
//        }
//        String newLine = getNewString();
//        list.set(getLineNumber()-1, newLine);
//        Writer writer = null;
//
//        try{
//            writer = new BufferedWriter(new FileWriter(getPath()));
//            for (String lines : list ){
//                writer.write(lines);
//                writer.write(System.getProperty("line.separator"));
//            }
//            writer.flush();
//        } catch (Exception e){
//            Logger.getLogger(StringParcer.class.getName()).log(Level.SEVERE, null, e);
//        } finally {
//            if (writer != null){
//                try {
//                    writer.close();
//                }catch (IOException ex){}
//            }
//        }
//    }


    public void changeLine () throws IOException {
        int lineNum = getLineNumber();
        File file = new File(getPath());
        File fileold = new File("/tmp/test2.txt");
        InputStream fileInputStream = new FileInputStream(file);
        OutputStream outputStream = new FileOutputStream(file);
        String line = null;

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));

        int count = 0;
        while ((line = br.readLine()) != null) {
            int cnt = count;

            if (cnt == getLineNumber() - 1) {
                outputStream.write(newString.getBytes());
                outputStream.write(System.getProperty("line.separator").getBytes());

            } else {
                outputStream.write(line.getBytes());
                outputStream.write(System.getProperty("line.separator").getBytes());
            }
            count++;
        }
    }
//        InputStream old = new FileInputStream("/tmp/test2.txt");
//        OutputStream renew = new FileOutputStream(path);
//
//        for (int b = old.read();b >=0 ; b = old.read() ){
//            renew.write(b);
//        }
//        fileold.delete();
//    }

    public static void main(String[] args) throws IOException {

        StringParcer parcer = new StringParcer(4,"/tmp/test.txt","Хуйтама");
        parcer.changeLine();

        System.out.println("Parse finished");




    }
}
