/*package com.videal.inventarisation;


import org.junit.Test;
import com.videal.inventarisation.dao.ComputerService;
import com.videal.inventarisation.dao.Computers;
import java.util.List;

public class InventrApplicationTests {

	ComputerService service = new ComputerService();

	@Test
	public void testSaveRecord() throws Exception {
		//Создаем автомобиль для записи в БД
		Computers comp1 = new Computers();
		comp1.setHostName("TestComp1");
		comp1.setIp("192.168.0.1");
		comp1.setOs("Linux");
		comp1.setMotherboard("Asus P753L");
		comp1.setCpu("Intel i5");
		comp1.setRam("4GB");
		comp1.setHdd("500GB");
		//Записали в БД
		Computers comps = service.add(comp1);

		//Вывели записанную в БД запись
		System.out.println(comps);
	}

	@Test
	public void testDeleteRecord() throws Exception {
		Computers comp1 = new Computers();
		comp1.setHostName("TestComp1");
		comp1.setIp("192.168.0.1");
		comp1.setOs("Linux");
		comp1.setMotherboard("Asus P753L");
		comp1.setCpu("Intel i5");
		comp1.setRam("4GB");
		comp1.setHdd("500GB");
		//Записали в БД
		Computers comps = service.add(comp1);

		//Удвлем его с БД
		service.delete(comps.getId());
	}

	@Test
	public void testSelect() throws Exception {
		//Создаем автомобиль для записи в БД
		Computers comp1 = new Computers();
		comp1.setHostName("TestComp1");
		comp1.setIp("192.168.0.1");
		comp1.setOs("Linux");
		comp1.setMotherboard("Asus P753L");
		comp1.setCpu("Intel i5");
		comp1.setRam("4GB");
		comp1.setHdd("500GB");
		//Записали в БД
		Computers comps = service.add(comp1);

		//Получние с БД Citroen‎
		Computers compsFromDB = service.get(comps.getId());
		System.out.println(compsFromDB);
	}

	@Test
	public void testUpdate() throws Exception {
		//Создаем автомобиль для записи в БД
		Computers comp1 = new Computers();
		comp1.setHostName("TestComp1");
		comp1.setIp("192.168.0.1");
		comp1.setOs("Linux");
		comp1.setMotherboard("Asus P753L");
		comp1.setCpu("Intel i5");
		comp1.setRam("4GB");
		comp1.setHdd("500GB");
		//Записали в БД
		Computers comps = service.add(comp1);

		comp1.setIp("192.168.0.2");

		//Обновляем
		service.update(comp1);

		//Получаем обновленую запись
		Computers comp2 = service.get(comp1.getId());
		System.out.println(comp2);
	}

	@Test
	public void testGetAll(){
		//Создаем автомобиль для записи в БД
		Computers comp1 = new Computers();
		comp1.setHostName("TestComp1");
		comp1.setIp("192.168.0.1");
		comp1.setOs("Linux");
		comp1.setMotherboard("Asus P753L");
		comp1.setCpu("Intel i5");
		comp1.setRam("4GB");
		comp1.setHdd("500GB");

		//Создаем автомобиль для записи в БД
		Computers comp2 = new Computers();
		comp2.setHostName("TestComp1");
		comp2.setIp("192.168.0.1");
		comp2.setOs("Linux");
		comp2.setMotherboard("Asus P753L");
		comp2.setCpu("Intel i5");
		comp2.setRam("4GB");
		comp2.setHdd("500GB");

		//Создаем автомобиль для записи в БД
		Computers comp3 = new Computers();
		comp3.setHostName("TestComp1");
		comp3.setIp("192.168.0.1");
		comp3.setOs("Linux");
		comp3.setMotherboard("Asus P753L");
		comp3.setCpu("Intel i5");
		comp3.setRam("4GB");
		comp3.setHdd("500GB");

		//Сохраняем все авто
		service.add(comp1);
		service.add(comp2);
		service.add(comp3);

		//Получаем все авто с БД
		List<Computers> computers = service.getAll();

		//Выводим полученый список авто
		for(Computers c : computers){
			System.out.println(c);
		}
	}

}
*/