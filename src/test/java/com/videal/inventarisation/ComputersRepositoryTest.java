package com.videal.inventarisation;
import com.videal.inventarisation.db.models.Computers;
import com.videal.inventarisation.db.repositories.IComputerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import java.util.List;

/**
 * Created by oleg on 11/15/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = InventrApplication.class)
@WebAppConfiguration
public class ComputersRepositoryTest {
    @Qualifier("IComputerRepository")
    @Autowired
    IComputerRepository iComputerRepository;
    @Test
    public void testComputers(){


        List<Computers> computers = iComputerRepository.findAll();

        for(Computers c : computers){
            System.out.println(c);
        }
    }

}
