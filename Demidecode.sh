#!/bin/bash

file=/tmp/demidecode.info
dst_url=http://zabbix.videal.net:8181/upload

system_serial=$(dmidecode --type 1| grep 'Serial Number:'|awk '{print $3}')
system_manufacturer=$(dmidecode --type 1| grep Manufacturer:|awk '{print $2}')
system_version=$(dmidecode --type 1| grep 'Version:'|awk '{print $2}')
motherboard_prname=$(dmidecode --type 2| grep 'Product Name:'|awk '{print $3}')
motherboard_sn=$(dmidecode --type 2| grep 'Serial Number:'|awk '{print $3}')
#cpu_version=$(dmidecode --type 4|grep Version:|cut -f 2 -d :)
cpu_version=$(cat /proc/cpuinfo |grep -m1 'model name'|cut -f 2 -d :)
os=$(lsb_release -a|grep Description:|awk '{print $2}')
hostname=$(hostname)
showInterfaces=$(lshw -C network |grep -i 'logical name\|логическое имя' | awk '{ print $3 }')
ramSlotNumber=$(dmidecode --type 17|grep -i 'Memory Device'|grep -v grep |wc -l)
uuid=$(dmidecode | grep UUID|awk '{print $2}')

ramSize=$(dmidecode --type 17|grep -i 'Size:\|размер' |grep -v grep|awk '{print $2}')
if [[ -z "$ramSize" ]]; then
  ramSize=$( lshw -c memory|grep -A 20 '*-memory'|grep size|cut -f 2 -d : )
fi
    ramSizeCount=0
    for i in $ramSize;
    do
    ramSizeArr[${ramSizeCount}]=${i}
    (( ramSizeCount++ ))

    done


ramType=$(dmidecode --type 17|grep -i 'Type:'|grep -v grep|awk '{print $2}')
    ramTypeCount=0
    for i in $ramType;
    do
     ramTypeArr[${ramTypeCount}]=${i}
    (( ramTypeCount++ ))
    done

ramSerial=$(dmidecode --type 17|grep -i 'Serial Number:'|grep -v grep|awk '{print $3}')
    ramSerialCount=0
    for i in $ramSerial;
    do
    ramSerialArr[${ramSerialCount}]=${i}
    (( ramSerialCount++ ))
    done

for (( i = 0 ; i < ${ramSizeCount}; i++ ))
 do
    ram[${i}]="{\"size\":\"${ramSizeArr[${i}]}\",\"type\":\"${ramTypeArr[${i}]}\",\"serial\":\"${ramSerialArr[${i}]}\",\"slot\":\"${i}\"},"
done

old_IFS=$IFS      # save the field separator
IFS=$'\n'
hddName=$(lshw -c disk|grep -i 'product\|продукт'|cut -f 2 -d:)
    hddNameCount=0
    for i in $hddName;
    do
    hddNameArr[${hddNameCount}]="${i}"
    (( hddNameCount++ ))
    done
IFS=$old_IFS

hddSize=$(lshw -c disk|grep 'size:\|размер:'|cut -f 2 -d:|awk '{print $2}'|sed "s/(//g;s/)//g")
    hddSizeCount=0
    for i in $hddSize;
    do
    hddSizeArr[${hddSizeCount}]=${i}
    (( hddSizeCount++ ))
    done

hddSerial=$(lshw -c disk|grep 'serial:\|серийный..:'|cut -f 2 -d:)
    hddSerialCount=0
    for i in $hddSerial;
    do
    hddSerialArr[${hddSerialCount}]=${i}
    (( hddSerialCount++ ))
    done

for (( i = 0 ; i < ${hddSerialCount}; i++ ))
 do
    hdd[${i}]="{\"product\":\"${hddNameArr[${i}]}\",\"size\":\"${hddSizeArr[${i}]}\",\"serial\":\"${hddSerialArr[${i}]}\"},"
done

intCount=0
for i in $showInterfaces;
   do
    if [[ -n  $(ifconfig ${i}|grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}') ]]
    then
	interface[${intCount}]=${i}
	ip[${intCount}]=$(ifconfig ${i}|grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
	((intCount++))
	fi 
done

for (( i = 0 ; i < ${intCount}; i++ ))
 do
    jsinterface[${i}]="{\"netinterface\":\"${interface[${i}]}\",\"ip\":\"${ip[${i}]}\"},"
done

printf '{ "hostname":"%s","os":"%s","systemmanufacturer":"%s","motherboardprname":"%s","pcname":"%s","motherboardsn":"%s","uuid":"%s","cpu":"%s","network":[%s],"ram":[%s],"hdd":[%s] }\n'\
        "$hostname" "$os" "$system_manufacturer" "$motherboard_prname" "$system_version" "$motherboard_sn" "$uuid" "$cpu_version" "${jsinterface[*]}" "${ram[*]}" "${hdd[*]}" > $file

sed -i 's/^[ \t]*//' $file
sed -i 's/\},\]/\}\]/g' $file
curl -i -F name=$(hostname) -F file=@$file $dst_url
